#include  <stdio.h>

int main () {


	int res;  // result of 'scanf'
	int feet =0, inches =0 ;
	float santimeters = 0.0f;
	
	int i =0;
	for (i=0;;i++) {	
		puts ("Please enter number of feet and inches in the following format: feet'inches");

		res =  scanf ("%d'%d", &feet, &inches);
		fflush (stdin); 
		if (res ==2) {
			printf ("Equals %.2f santimeters.\n", (12*feet + inches)*2.54);
			break;
		}
		else  
			puts ("Wrong values. Please follow the formats");
	}
	return 0;
}