#include <stdio.h>

int main (){

	/*
	morning =  06:00:00 - 09:59:59 -- > number of second = 6 * 3600 :  09 * 3600 + 59 * 60 + 59
	daytime =  10:00:00 - 18:59:59
	evening =  19:00:00 - 22:59:59
	night   =  00:00:00 - 05:59:59
	night   =  23:00:00 - 23:59:59
	*/
	char message1 []= "Good morning!";
	char message2 []= "Good afternoon!";
	char message3 []= "Good evening!";
	char message4 []= "Good night!";
	
	int morningMIN = 6 * 3600;
	int morningMAX = 9 * 3600 + 59 * 60 + 59;

	int daytimeMIN = 10 * 3600;
	int daytimeMAX = 18 * 3600 + 59 * 60 + 59;

	int eveningMIN = 19 * 3600;
	int eveningMAX = 22 * 3600 + 59 * 60 + 59;

	int night1MIN = 23 * 3600;
	int night1MAX = 23 * 3600 + 59 * 60 + 59;

	int night2MIN = 00 * 3600;
	int night2MAX = 05 * 3600 + 59 * 60 + 59;
	
	int i=0;
	int res = 0;
	int result =0;
	int hh=0, mm =0, ss=0;
	

	for (i=0;;i++) {
		puts ("Please enter the time in defined format HH:MM:SS");
		res =scanf ("%i:%i:%i", &hh,&mm,&ss);
		fflush (stdin);
		if ( res != 3 || hh<0 || hh>23	 || mm <0 || mm> 59 || ss<0 || ss>59  )
			puts ("You entered wrong value! Please strictly follow the specified format.");
		else
			break;
	}
	result = hh* 3600 + mm* 60 + ss;
	if (result < morningMAX && result >morningMIN)
		printf (message1);
	else if (result < daytimeMAX && result >daytimeMIN)
		printf (message2);
	else if (result < eveningMAX && result >eveningMIN)
		printf (message3);
	else if (result < night1MAX && result >night1MIN)
		printf (message4);
	else if (result < night2MAX && result >night2MIN)
		printf (message4);
	return 0;
}