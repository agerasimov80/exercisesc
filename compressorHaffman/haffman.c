# include <stdio.h>
# include <string.h>
# include <stdlib.h>
# include <locale.h>
# include "STRUCT.h"
# include "TREE.h"
# include "SYMS.h"
# define SIZE 255

int main () {

	char * sign;
	union CODE un ;
	unsigned int bits [8];
	FILE * f, * f1, * f2;
	int c=0, i=0, countSYMs=0, countCh=0, tl=0;
	struct SYM sym [SIZE], * psyms[SIZE] , * root;
	
	setlocale(LC_ALL,"rus");
	clearSYMs(sym);							// ������� ������ ��������
	
	f= fopen ("input1.txt","rt");
	countCh=countChars(f);					
	fseek(f,0,SEEK_SET);

	// ������ ���� ����������� � ��������� ������
	// ��������� ������ �������� � ������� ��������� ������� createSYMs
	while (1) {									
		 c = (int) fgetc(f);			// !!! 'c' ��������� ��� int ����� ���������� ����� ����� !!!
		 if (c==EOF) break;
		 countSYMs = createSYMs (c, sym, countCh);
	} ;
	printSYMs(sym,countSYMs);

	// ��������� ������ �������� � ������� ��������� ������� sortSYMs
	// � �������� ���������
	sortSYMs_BubbleSort(sym,countSYMs);
	printSYMs(sym,countSYMs);
	
	// ������� ������ ���������� �� ��������� SYMs
	for (i=0;i<SIZE;i++)
		psyms[i]=&sym[i];

	// ������ ������ �������� � �������� ��������� �� ��� ������.
	root = buildTree(psyms,countSYMs);

	// ���������� ���������� ���� ������� �������� ������.
	makeCodes(root);
	
	// ������ ������ ������� ���� f ����������� 
	// ���� ������ � ������� �������� SYM [SIZE]
	// ������� ������ ���������� ���������� ��� 
	// ���������� ��� � ����.
	f1 = fopen("output.txt","wt");
	if (f1==NULL) return -1;
	
	fseek(f,0,SEEK_SET);
	while (1) {									
			c = (int) fgetc(f);			// !!! 'c' ��������� ��� int ����� ���������� ����� ����� !!!
			if (c==EOF) break;
			fputs(findCode(sym, c),f1);			 
	}
	
	fclose(f1);
	fclose(f);

	// ��������� �������� ���� ��� ������
	f2=fopen ("final.txt","wb");
	
	// ���������� �������.  3 �����
	sign = "haf";
	fwrite (sign,1, 3,f2);	

	// ���������� ������ ������� �������������  1 ����
	// � ���� ������� ������������� 
	fwrite ( &countSYMs, 1, 1,f2);
	i=0;
	while (i<countSYMs) {
		fwrite ( &sym[i].ch, 1, sizeof(unsigned char),f2); // ������
		fwrite ( &sym[i].freq, 1, sizeof(float),f2); // ���
		i++;
	}

	// ������ ���� � ������ � ���������� �������� � �������� ����
	f1=fopen ("output.txt","rt");
	while (1) {									
		// ������� ���� �����
		un.ch = un.ch & 0;
		// ������� ������ �����
		// � ����� ������ ���� � ������� �� ����� � ������ ����� �� ����� �����
		for (i=0; i<8; i++)
			bits[i]=0;
		for (i=0; i<8; i++) {
			c= (unsigned int) fgetc(f1);
			if (c==EOF) {
				tl = 8-i;
				break;
			}
			bits[i]=c;
		}
		// ���������� ���� � ���� �����
		un.byte.b1=bits[0]-'0';
		un.byte.b2=bits[1]-'0';
		un.byte.b3=bits[2]-'0';
		un.byte.b4=bits[3]-'0';
		un.byte.b5=bits[4]-'0';
		un.byte.b6=bits[5]-'0';
		un.byte.b7=bits[6]-'0';
		un.byte.b8=bits[7]-'0';
		// ���������� 1 ���� ���������� � �������� ������ � �������� ����
		fwrite (&un.ch,1, 1,f2);	
		if (c == EOF)
			break;	
	}
	fclose(f1);

	// ���������� ����� ������
	fwrite (&tl,1, 1,f2);	
	// ���������� ������ ���


	fclose(f2);


return 0 ;
}