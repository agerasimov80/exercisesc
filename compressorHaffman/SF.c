// ������� ��������� ��������� �� ������ �������� SYM � ����� �������
// ��������� ������ ����������� '��������'
// ������������ ��������� ������� swapSYMs
void sortSYMs_BubbleSort(struct SYM * sym, int n){
	int i=0, j=0;
	struct SYM *temp;
	struct SYM *tempNext;

	temp=sym;
	while (i<n) {		
		tempNext=temp;
		tempNext++;
		j=i+1;
		while (j<n) {
			if (temp->freq<tempNext->freq)
				swapSYMs(temp,tempNext);
			tempNext++;
			j++;
		}
		temp++;
		i++;
	}
}