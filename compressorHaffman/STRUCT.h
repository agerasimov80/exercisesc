# define SIZE 255

// ��������� SYM
struct SYM
{
	unsigned char	ch;			// ACSII ��� 
	float			freq;		// frequency
	char			code[SIZE];	// array for new string code
	struct SYM		*left;		// left derivate
	struct SYM		*right;		// right derivate
};

union CODE
{
	unsigned char ch;
	struct BYTE
	{
		unsigned b1:1;
		unsigned b2:1;
		unsigned b3:1;
		unsigned b4:1;
		unsigned b5:1;
		unsigned b6:1;
		unsigned b7:1;
		unsigned b8:1;
	} byte;
};