#include <stdlib.h>
#include "STRUCT.h"
#include "SYMS.h"
#include "TREE.h"

// ������� ������ ������ �������� �������� �� ���� ������ �������� 
// � ������ ��������� - ��� ������� � ��� �������.
struct SYM* buildTree (struct SYM *psym[], int n) {

	// ������� ��������� ����
	struct SYM * temp =(struct SYM *) malloc (sizeof(struct SYM));
	// � ���� ������� ����������� ����� ������
	// ���������� � �������������� ��������� ������� psym
	temp->freq=psym[n-2]->freq + psym [n-1]->freq;
	// ��������� ��������� ���� � ����� ���������� ������ 
	temp ->left=psym[n-1];
	temp ->right=psym[n-2];
	temp ->ch = ' ';
	temp ->code[0]=0;

	if (n==2) // �� ������������ �������� ������� � �������� 1.0
		return temp;

	// �������� ��������� �� ��������� ������� psym,  ���� ������� ������ �� �����
	psym [n-1]= NULL;

	// ��������� ����� ��������� �������: 
	// � ��������� �� ������������� ������� ���������� ����� ���������� ����
	// � ��������� ������������ ������ ���������� psym ������� �������, 
	// �������� ������� �������� �������.
	psym [n-2] = temp;

	sortSYMs_InsertionSort (psym , n-1);

	//printPSYMs (psym ,n-1);

	return buildTree(psym, n-1);
}

// ��������� ��������� ����������� ���� 
// ��� ������� ������, ������� ������� ���������� �� ������� ����� 
// ������ � ��������  0 ��� 1 � ���������� �� ����� ��� ������ �����
void makeCodes (struct SYM  *root) {
	if (root->left) {
		strcpy (root->left->code,root->code);
		strcat (root->left->code, "0");
		makeCodes (root->left);
	}
	if (root->right) {
		strcpy (root->right->code,root->code);
		strcat (root->right->code, "1");
		makeCodes (root->right);
	}
}
