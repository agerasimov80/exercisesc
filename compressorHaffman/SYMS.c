# include "SYMS.h"
# include "STRUCT.h"
# include <stdio.h>

void sortSYMs_InsertionSort (struct SYM * psym [], int n){
	int j=n-1;
	struct SYM * temp;

	while (j>0 && (psym[j]->freq > psym[j-1] ->freq) )  {
		temp = psym[j];
		psym [j] = psym[j-1];
		psym[j-1] = temp;
		j--;
	}
}

void printSYMs(struct SYM * sym, int n) {
	int i=0;

	while (i<n){
		if (sym->ch == '\n') 
			printf ("%02d..\\n..%f\n",i,sym->freq);
		else
			printf ("%02d..%c..%f\n",i,sym->ch,sym->freq);
		sym++;
		i++;
	}
}

// --------------------------------------------------------------------------
// ������� ��������� ��������� �� ������ �������� SYM, 
// � ����� ����� ����������� ������.
// ������� ������������� ������� ������������� �������, � ������ ���� ������ 
// �� ����� � ���������� ����� �������.
// ���� ������ ����� - ����������� ������ � ��������� ���������.
//--------------------------------------------------------------------------
int createSYMs (unsigned char c, struct SYM * sym, int countCh) {
	int count=0;
	struct SYM * temp =sym;

	while (sym->ch!=c && sym->ch!=' ') {
		sym++; 
	}

	sym->ch=c;
	sym->freq= (((sym->freq) *countCh+1)/ countCh) ;
	
	while (temp->ch != ' ') {
		temp++;
		count++;
	}
	return count;
}

// ������� ������� ��� ��������� SYM
void clearSYMs(struct SYM * sym){
	int i=0;
	while (i<SIZE)
	{
		sym->ch=' ';
		sym->code[0]=0;
		sym->freq=0;
		sym->left=NULL;
		sym->right=NULL;
		i++;
		sym++;
	}
}
// ��������� ������� ��������� ��� ��������� �� ��� ��������� SYM
void swapSYMs(struct SYM* sym1, struct SYM * sym2){
	
	unsigned char c;

	c = sym1->ch;
	sym1->ch = sym2->ch;
	sym2->ch = c;

	sym1->freq =  sym1->freq+ sym2->freq;
	sym2->freq =  sym1->freq- sym2->freq;
	sym1->freq =  sym1->freq- sym2->freq;
}

// ������� ��������� ��������� �� ������ �������� SYM � ����� �������
// ��������� ������ ����������� '��������'
// ������������ ��������� ������� swapSYMs
void sortSYMs_BubbleSort(struct SYM * sym, int n){
	int i=0, j=0;
	struct SYM *temp;
	struct SYM *tempNext;

	temp=sym;
	while (i<n) {		
		tempNext=temp;
		tempNext++;
		j=i+1;
		while (j<n) {
			if (temp->freq<tempNext->freq)
				swapSYMs(temp,tempNext);
			tempNext++;
			j++;
		}
		temp++;
		i++;
	}
}


void printPSYMs(struct SYM * psym [], int n) {
	int i=0;

	while (i<n){
		if (psym[i]->ch == '\n') 
			printf ("%02d..\\n..%f\n",i,psym[i]->freq);
		else
			printf ("%02d..%c..%f\n",i, psym[i]->ch,psym[i]->freq);
		i++;
	}
}

// ��������� ������� ��� �������� ������ ���������� �������� � �����.
int countChars(FILE * f) {
	int count =0 , c;
	do {
		c=fgetc(f);
		count++;
	} while (c !=EOF);

	return count;
}

// ������� ������ ������� � �����,
// ���������� �������������� ���������� ���
char * findCode (struct SYM * sym, unsigned char c) {
	int i=0;
	while (i<SIZE) {
		if (sym->ch==c) break;
		sym++;
	}
	return sym->code;
}
