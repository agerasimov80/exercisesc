# include <stdio.h>


// --------------------------------------------------------------------------
// ������� ��������� ��������� �� ������ �������� SYM, 
// � ����� ����� ����������� ������.
// ������� ������������� ������� ������������� �������, � ������ ���� ������ 
// �� ����� � ���������� ����� �������.
// ���� ������ ����� - ����������� ������ � ��������� ���������.
//--------------------------------------------------------------------------
int createSYMs (unsigned char c, struct SYM * sym, int countCh) ;

// ������� ������� ��� ��������� SYM
void clearSYMs(struct SYM * sym);

// ��������� ������� ��� �������� ������ ���������� �������� � �����.
int countChars(FILE * f) ;

void sortSYMs_BubbleSort(struct SYM * sym, int n);
void sortSYMs_InsertionSort (struct SYM * psym [], int n);
void swapSYMs(struct SYM* sym1, struct SYM * sym2);

// ������� ��������� ��������� �� ������ �������� SYM
// � ������� � ����� stdout, ����� �������, ��� ������ � ��� �������.
void printSYMs(struct SYM * sym, int n);
void printPSYMs(struct SYM * psym [], int n) ;


// ������� ������ ������� � �����,
// ���������� �������������� ���������� ���
char * findCode (struct SYM * sym, unsigned char c) ;
