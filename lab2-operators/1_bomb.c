#include <stdio.h>
#include <time.h>

int main () {

	float initial_height = 0.0f, height=0.0f, length =0.0f;
	const float g =9.81f;
	int sec_count=0;
	int res =0;
	clock_t now = clock();			// got starting point time

	// requesting for the initial height
	while (res!=1) {
		puts("Please enter initial height");
		res=scanf("%f", &initial_height);
		fflush(stdin);
	}
	
	height= initial_height;	// got initial height

	while (height>0){
		
		printf ("t=%02d seconds h=%04f meters\n", sec_count,height);
		
		//delay by 1000 milliseconds
		while (clock() < now + 1000)	
		{
			;						
		}
		
		// performing calculations
		sec_count++;
		length = (g * sec_count * sec_count)/2;
		height =initial_height- length;
	}
	puts ("End");

	while (clock() < now + 5000)	
		{
			;						
		}

	return 0;
}