#include <stdio.h>
#include <string.h>


void chomp(char buf[]) {
	if(buf[strlen(buf)-1]=='\n')
		buf[strlen(buf)-1]=0;
}

int main () {

	char buf [256];
	int s=0, i=0, j=0;
	puts ("Please enter a string not greater than 256 characters");
	fflush(stdin);
	fgets(buf,256,stdin);
	fflush(stdin);
	
	chomp (buf);
	s=strlen(buf);
	
	//asci 0 = 48
	//asci 9 = 57
	//asci A = 65
	//asci z = 122

	// 'bubble sorting'
	for (i;i<s;i++){
		j=0;
		for(j;j<s-i-1;j++)
			if (buf[j]>buf[j+1]){
				buf[j] = buf[j] +buf[j+1];
				buf[j+1] = buf[j] - buf[j+1];
				buf[j] = buf[j] - buf[j+1];
			}
	}

	for (i=0;i<s;i++)
		printf("%c",(char)buf[i]);
	puts ("\n");
	return 0;
}