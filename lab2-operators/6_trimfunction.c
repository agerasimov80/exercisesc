#include <stdio.h>
#include <string.h>

void chomp(char buf[]) {
	if(buf[strlen(buf)-1]=='\n')
		buf[strlen(buf)-1]=0;
}
void pulloutacharacter(char buf[], int c) {
	int i=0;
	for (i=c;i<strlen(buf)-1;i++)
		buf[i]=buf[i+1];
	buf[strlen(buf)-1]=0;
}


int main (){

	char buf [256] ;
	int i=0;

	printf("Please enter a string: ");
	fflush(stdin);
	fgets(buf,256, stdin);
	chomp(buf);

	
	i = 0;
	while (buf[i]!=0) {
		if (buf[i]==32 && buf[i+1]==32){	// cut-off duplicated spaces
			while (buf[i+1]==32)
				pulloutacharacter(buf,i);
		}
		if(buf[i]==32 && i==0){				// cut-off first space
			while (buf[i]==32)
				pulloutacharacter(buf,i);
		}
		if(buf[i]==32 && buf[i+1]==0){				// cut-off last space
				pulloutacharacter(buf,i);
		}
		i++;
	}

	// print the result
	for (i=0;i<strlen(buf);i++)
		printf("%c",buf[i]);
	

	return 0;
}