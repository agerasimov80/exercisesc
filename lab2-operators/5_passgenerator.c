#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {

	const int length =8;
	const int numberofpasswords = 10;
	char part1min ='a', part1max='z';
	char part2min ='A', part2max='Z';
	char part3min ='0', part3max='9';
	int rand_val=0;
	int i=0, j=0;
	char c='a';

	//set up randomizer
	srand(time(0));
	fflush(stdin);
	puts ("Please enter number of passwords: ");
	scanf("%d", &numberofpasswords);
	fflush(stdin);
	puts ("Please enter passwords' length: ");
	scanf("%d", &length);
	fflush(stdin);
	puts("Here you are!");

	for(j=0;j<numberofpasswords;j++){

		for (i=0;i<length;i++) {
			// randomly select the universe
			rand_val =rand()%4;
			if (rand_val ==0)
				c ='a'+ rand()%26;	// randomly select a small character (26 variants)
			else if (rand_val ==1)	
				c ='A'+ rand()%26;	// randomly select a capital character (26 variants)
			else if (rand_val ==2)	
				c ='0'+ rand()%10;	// randomly select a digit (10 variants)
		
			printf("%c", c);
		}
		printf("\n");
	}

	return 0;
}