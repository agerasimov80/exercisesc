#include <stdio.h>
#include <string.h>

void chomp (char buf[]){
	if (buf[strlen(buf)-1]=='\n')
		buf[strlen(buf)-1]=0;
}




int main () {
	char buf[256];
	int hashtable [256][256];
	int i=0;
	int j=0;

	for (i=0;i<256;i++) {
		for (j=0;j<256;j++)
			hashtable [i][j]=0;
	}
	fflush(stdin);
	printf("Please enter a string: ");
	fgets(buf, 256, stdin);
	fflush(stdin);
	chomp(buf);

	i=0;
	while (buf[i]){
		// lookup the character in the hashtable
		j=0;

		// search for the next empty element in the hashtable
		while (hashtable[j][0] && (buf[i]!=hashtable[j][0]) ){ 
			j++;
		}
		if (hashtable[j][0]!=(int)buf[i])
			hashtable[j][0]=(int)buf[i];
		hashtable[j][1]++;
		i++;
	}
	
	// print table of sequence
	j=0;
	while (hashtable[j][0])	{
		printf ("Character: %c, sequence: %d\n", (char) hashtable[j][0], hashtable[j][1]);
		j++;
	}

		return 0;
}