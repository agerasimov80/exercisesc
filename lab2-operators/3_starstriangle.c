#include <stdio.h>

int main (){
	
	
	// resuest for input from the user
	int numL, numT,i,j, s;
	printf("Enter lines number: ");
	scanf("%d",&numL);
	for (;;) {
		
		//puts ("Number of lines & triangles should meet theformula ((lines-1)*2 +1)*triangels < 80");	
		printf("Enter number of triangles from 1 to %d:", 80/((numL-1)*2 +1));
		scanf("%d",&numT);
		if (((numL-1)*2 +1)*numT >80) 
			puts ("Try again");
		else 
			break;
	} 

	s=0;
	for (;s<((numL-1)*2 +1)*numT;s+=((numL-1)*2 +1)) {
		// draw a triangle
		for(i=0;i<numL;i++) {
			for(j=s; j<=s+numL+i-1;j++)
				if (j<s+numL-i-1)
					putchar(' ');
				else 
					putchar('*');
			putchar('\n');
		}
	}
	return 0;
}