#include <stdio.h>
# include <time.h>
# include <stdlib.h>

#define N 20
#define M 80

void clearSky (char s [N] [M]) {
	int i,j;
	for (i=0;i<N;i++)
		for (j=0;j<M;j++)
			s[i][j]=' ';
}
void drawQuarterSky (char s[N][M]) {
	int i,j, shuffle;
	srand (time (0));
	for (i=0;i<=N/2-1;i++)
		for (j=0;j<=M/2-1;j++){
			shuffle= rand ()%2;
			if (shuffle ==1) 
				s[i][j]='*';
		}
}
void copyQuarterSky (char s[N][M]) {
	int i,j;
	for (i=0;i<=N/2-1;i++)
		for (j=0;j<=M/2-1;j++){
			s[i][M-j-1]=s[i][j];
			s[N-i-1][j]=s[i][j];
			s[N-i-1][M-j-1]=s[i][j];
		}
}
void clearScreen () {
	int i,j;
	for (i=0;i<N;i++)
		for (j=0;j<M;j++)
			putchar (' ');
		putchar ('\n');
}
void drawSky(char s[N][M]){
	int i,j;
	for (i=0;i<N;i++){
		for (j=0;j<M;j++)
			putchar (s[i][j]);
		putchar ('\n');
	}
}

int main () {
	char s [N][M];
	int sec_count=0;
	clock_t now = clock();			// got starting point time
	srand(time(0));					// initialize rand function
		
	for(;;) {
		clearSky(s);
		drawQuarterSky(s);
		copyQuarterSky(s);
		clearScreen();
		drawSky(s);
		now = clock();
		while (clock() < now + 3000);	// limmiseconds
	}
	return 0;
}


