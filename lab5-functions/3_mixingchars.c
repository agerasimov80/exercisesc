#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define LINES_MAX 256
#define LINE_LENGTH_MAX 500
#define WORDS_MAX 100
#define WORDSIZE_MAX 50
#define INPUTFILENAME "input.txt"
#define OUTPUTFILENAME "output.txt"

void chomp (char * s) {
	if (s[strlen (s) -1]=='\n') (s[strlen (s) -1]='\0');
}

int shufflecompare (const void * a, const void * b){
	int rand_val=0;   
	srand(time(0));			// set up random function
    rand_val =rand()%2;		// initialize the number 1....100
	if ( * (char*)a =='-' || * (char*)b =='-' )	// the position of  '-' in a word will remain
		return 0	;	
	else  
		return ((rand_val == 0)? (1) : (-1));
}

void mixChars (char ** s  ) {		// s - pointer to a pointer to a word []
	int i=0;
	char * wordStart;
	char chars [WORDSIZE_MAX], * temp;

	if (**s== '"' || **s== '\'') 
		(*s)++;
	wordStart=temp=*s;
	temp++;								// one step forward
	if (*temp!= ' ') {					// in case of 1 char - do nothing
		while (
			*temp!='\0'		&&  
			* temp!=' '		&& 
			*temp!='\n'		&&
			*temp!='!'		&&
			*temp!='?'		&&	
			*temp!=','		&&
			*temp!='.'		&&
			*temp!=';'		&&
			*temp!='"'		&&
			*temp!='\''		
			) {
			chars[i++]= *temp++ ;
		}

		qsort (chars, i-1 , sizeof (char), shufflecompare); 
	
		while (i)  *(&(**s)+sizeof(char)*i) =chars[(i--)-1];
	}
}

int parseString (char * s [WORDS_MAX],  char * str) {
	/*
	TODO:
	1.	parse ! , .   at the end of the words - DONE
	2.	parse " "  ' ' around the words - DONE
	3.	check if 1 symbol word is parsed correctly - DONE
	4.	check if empty line is parsed correctly - DONE
	5.	check if more than 256 lines file is parsed correctly - DONE
	6.	parse compaund words correctly    word-word   - QUESTION
	*/
	int inWord=0, count =0 ;
	while (*str) {
		if (inWord==0 && *str!=' ') {	//faced a word
			inWord=1;
			s[count]=str;
			count++;
		}
		if (inWord==0 && *str==' ')	;	//continue spaces
		if (inWord!=0 && *str!=' ') ;	//continue a word
		if (inWord!=0 && *str==' ') {	//ends a word
			inWord=0;
		}
		str++;
	}
	return count;
}

int main () {
	char  str [LINES_MAX] [LINE_LENGTH_MAX];
	char * words [WORDS_MAX];
	int i=0, j=0, countWords=0, countLines=0;
	FILE *fin, *fout;

	/* OPEN FILES */
	fin=fopen(INPUTFILENAME,"rt");
	fout=fopen(OUTPUTFILENAME,"wt");
	if(!fin || !fout) {
		printf("Error! (Can�t open text.txt)\n");
		return 1;
	}
	
	/* READ FROM THE FILE STRING BY STRING,
	*  PARSE EACH STRING INTO WORDS
	*  MIX CHARS IN EACH WORD, EXCEPT THE FIRST AND THE LAST CHARS
	*  THEN WRITE TO OUTPUT FILE.  */
	
	while(fgets(str[i],LINE_LENGTH_MAX,fin) && countLines<LINES_MAX) {
		countLines++;
		chomp (str[i]);
		countWords=parseString(words ,str[i]);
		j=0;
		while (j<countWords) {
			mixChars (&words[j]); 
			j++;
		}
		
		fputs(str[i],fout);
		fputc('\n',fout);
	}
	return 0;
}