# include <stdio.h>
# include <stdlib.h>
# include <time.h>
# include <string.h>

#define M 10
#define SIZE 256	// maximum string length
#define L 100	// maximum number of words

void chomp (char * s) {
	if (s[strlen (s) -1]=='\n') 
		(s[strlen (s) -1]='\0');
}

void printWord(char * s, char delimiter) {
	while (*s!= ' ' && *s!='\0') {
		putchar (*s);
		s++;
	}
	putchar (delimiter);
}
int getWords(char * s [L], char * str) {
	int inWord=0, count =0 ;
	while (*str) {
		if (inWord==0 && *str!=' ') {	//faced a word
			inWord=1;
			s[count]=str;
			count++;
		}
		if (inWord==0 && *str==' ')	;	//continue spaces
		if (inWord!=0 && *str!=' ') ;	//continue a word
		if (inWord!=0 && *str==' ') {	//ends a word
			inWord=0;
		}
		str++;
	}
	return count;
}
int shufflecompare (const void * a, const void * b){
	int rand_val=0;   
	srand(time(0));			// set up random function
    rand_val =rand()%3;		// initialize the number 1....100
	
	return ((rand_val == 0 || rand_val == 1 )? (-1) : 1);
	// return (rand_val == 0)? (-1) : ( (rand_val == 1 )? 0 : 1);
}

int main () {

	char  str [SIZE], * words [L];
	int count =0, i =0;

	puts ("Please enter a string");
	fflush (stdin) ;
	fgets (	str, 256 , stdin);
	chomp (str);
	count =getWords (words, str);

	qsort ( words, count, sizeof (char *), shufflecompare); 

	while (i<count-1) {
		printWord (words [i], ' ');
		i++;
	}
	printWord (words [i], '.');		// the last word ends with .
	putchar ('\n');
	return 0;
}