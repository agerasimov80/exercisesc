#include <stdio.h>
#include <stdlib.h>


# define N 256
# define M 5


int main () {
	
	char buf [N];
	char * ps;
	char * p [M];
	int count =0;
	int inWordflag;
	int numberofwords=0;

	do {
		printf("Please enter a word less than %d words \n", M);
		fflush (stdin);
		fgets(buf, N, stdin);
		fflush (stdin);
		// counting number of words - should be less than M
		ps =buf;
		inWordflag=0;
		while (*ps!='\n') {
			if (*ps!=' ' && inWordflag ==0)  {
				inWordflag=1;
				if (count<M) p[count] =ps;
				count++;
			}
			if (*ps==' ' && inWordflag ==0)  
				;
			if (*ps!=' ' && inWordflag ==1)  
				;
			if (*ps==' ' && inWordflag ==1)  
				inWordflag=0;
			ps++;
		}
	} while (count >=M);
	
	count --;
	for (count; count>=0 ;count--) {
		ps =p[count];
		while ( *ps!=' ' && *ps !='\n'){
			putchar (*ps);
			ps++;
		}
		putchar (' ');
	}
	putchar ('\n');
	return 0;
}
