#include <stdio.h>
#include <stdlib.h>
#include <string.h>

# define N 256
# define M 5


int main () {
	
	char buf [N];
	char * ps, * ps1;
	int flag=0;

	printf("Please enter a string (maximum 256 chars).\n", M);
	fflush (stdin);
	fgets(buf, N, stdin);
	fflush (stdin);
	// _ _ _ _ _ _ _  
	// 1 2 3 4 5 6 7
	ps=buf;
	ps1= &buf[strlen(buf)-2];
	
	do {
		if (*ps!=*ps1) {
			puts("Not a palindrom!");
			return 0;
		}
		ps++;
		ps1--;
	} while (ps<ps1);
	puts("It's a palindrom!");

	putchar ('\n');
	return 0;
}