#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#define N 10	// maximum number of strings
#define M 80	// maximum string length

int compare (const void *a, const void *b) {
	int w1 = strlen (* (char**)a);
	int w2 = strlen (* (char**)b);
	return (w1<w2 ? 1:-1);
}

void chomp (char * s) {
	if (s[strlen (s)-1] == '\n') s[strlen (s)-1] = 0;
}

int main () {

	char str [N] [M];
	char * s [N];
	int i, j;

	FILE *fin, *fout;
	fin=fopen("intext.txt","rt");
	fout=fopen("target.txt","wt");
	if(!fin || !fout) {
		printf("Error! (I can�t open text.txt)\n");
		return 1;
	}
	i=0;
	while(fgets(str[i],256,fin)) {
		s[i]=str[i]; 
		i++;
	}
	fclose(fin);

	//char * s [10] = { s[0]; s [1]; s [2]} ;
	qsort (s , i, sizeof(char *), compare);

	for (j=0; j<i; j++) {
		chomp (s[j]);
		fputs (s[j], fout);
		fprintf (fout,"\n");
	}
	fclose(fout);
	
	return 0;
} 