# include <stdio.h>
# include <conio.h>
# include <string.h>

#define N 256	// maximum number of relatives
#define M 35	// maximum length of the name

int main () {

	int n=0;
	int res=0, i=0;

	char s[N] [M];							// stores names
	int tempAge=0, youngestAge=0, oldestAge=0;	// store ages
	char * young, * old;			// point to the names of the youngest and the oldest persons

	// request for number of relatives
	do {
		puts("Please enter number of relatives");
		fflush (stdin);
		res =scanf  ("%d", &n);
		fflush (stdin);
	}while(res!=1);

	i=0;
	while (i<n) {
		puts ("Please enter a name. Then enter age");
		printf ("Name : ");
		scanf ("%s", s[i]);
		printf ("Age : ");
		scanf ("%d", &tempAge);
		if (tempAge < youngestAge || youngestAge==0 ) {
			youngestAge = tempAge;
			young = s[i];
		}
		if (tempAge > oldestAge || oldestAge==0) {
			oldestAge = tempAge;
			old = s[i];
		}
		i++;
	}
	
	printf ("Yougest person is %s, he/she is %d years old,\n", young, youngestAge );
	printf ("Oldest person is %s, he/she is %d years old.\n", old, oldestAge );

	return 0;
}