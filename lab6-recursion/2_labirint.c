# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <time.h>


#define SIZE_X_MAX 80
#define SIZE_Y_MAX 32
#define EXITS 50
#define REFRESHTIME  700 // millisec

int l =-1;
int r=1;
int u=-80;
int d=80;
char * plabirint;
char * exits [50];

void drawLabirint () {

	clock_t now = clock();	
	char * labirint =plabirint;
	int i =0, j =0;

	while (clock() < now + REFRESHTIME);	// milliseconds

	for (;i<SIZE_Y_MAX;i++)
		for (j=0;j<SIZE_X_MAX;j++)
			putc(*labirint++,stdout);
}
void scanExits () {

	char * labirint =plabirint;
	int i =0, j =0;
	int count=0;

	for (;i<SIZE_Y_MAX;i++)
		for (j=0;j<SIZE_X_MAX;j++) 
		{
			if (*labirint==' ' && (i==0 || i ==SIZE_Y_MAX-1 || j ==0 || j ==SIZE_X_MAX-3 )) 
				exits[count++] =labirint ;
			labirint++;
		}
}

int checkExit (char * curPos) {
	int i =0;

	for (;i<EXITS;i++) 
		if(curPos== exits[i] ) return 1;
	return 0;
}

char * read_labirint_from_file (FILE * f) {
	char * l;
	int countCol =0, countRow =0;
	char c= ' ';

	// GET LINE_SIZE
	char s [SIZE_X_MAX +1];
	while (fgets(s, SIZE_X_MAX +1,f)) {
		if (s[strlen(s)-1]=='\n' && strlen(s)!=SIZE_X_MAX) return NULL;
		if (s[strlen(s)-1]!='\n' && (strlen(s)+1)!=SIZE_X_MAX) return NULL;
		countRow++;
	}
	countCol = SIZE_X_MAX;
	if (countRow!=SIZE_Y_MAX) return NULL;
	
	l=(char *)malloc(countRow * countCol);
	
	fseek(f,0,SEEK_SET);
	do {
      c = fgetc (f);
	  *l++=c;
    } while (c != EOF);

	l-= (countRow * countCol);
	return l;
}

void putX ( char * curPos) {
		*curPos='X';
		drawLabirint ();
}
void delX ( char * curPos) {
		*curPos=' ';
		drawLabirint ();
}

int AnalysePosition(char * curPos) {

	char * oldPos =curPos;
	int flag =0;

	if (checkExit (curPos) )
	{
		exit (0);
		puts("Exit found!");
	}
	if ( *(curPos + l)==' ' ) {
		curPos+=l;
		putX (curPos);
		if (AnalysePosition (curPos) ==0)  {
			delX(curPos);
			curPos=oldPos;
		}
		else 
			flag =1;
	}
	if ( *(curPos + r)==' ' )  {
		curPos+=r;
		putX (curPos);
		if (AnalysePosition (curPos) ==0)  {
			delX(curPos);
			curPos=oldPos;
		}
		else 
			flag =1;
	}
	if ( *(curPos + u)==' ' )  {
		curPos+=u;
		putX (curPos);
		if (AnalysePosition (curPos) ==0)  {
			delX(curPos);
			curPos=oldPos;
		}
		else 
			flag =1;
	}
	if ( *(curPos + d)==' ' )  {
		curPos+=d;
		putX (curPos);
		if (AnalysePosition (curPos) ==0)  {
			delX(curPos);
			curPos=oldPos;
		}
		else 
			flag =1;
	}

	return flag;
}

int main () {

	char labirint [SIZE_X_MAX][SIZE_Y_MAX];
	//char *plabirint;
	FILE * flabirint;
	char * p=NULL;

	flabirint = fopen("labirint1.txt","rt");

	plabirint = read_labirint_from_file(flabirint);
	if (plabirint==0) return 1;

	drawLabirint () ;
	p=plabirint;

	while (*p++!='X'); 
	p--;
	scanExits ();
	AnalysePosition(p);

	return 0;
}