#include <stdio.h>
#include <stdlib.h>
#define SIZE 256

char  partition( char *buf, char *expr1, char *expr2) {
	// (1+  ((3+1)*(2+1)) )
	// (((3+1)*(2+1)) +1 )
	int count1 =0; // (
	int count2 =0; // )
	char op;
	buf++;
	while (*buf != '+' && *buf != '-' && *buf!= '*' && *buf  != '/' ||  count1!=count2) {
		if (*buf=='(') count1++;
		if (*buf==')') count2++;
		*expr1++=*buf++;
	}
	*expr1='\0';
	while (*buf != '+' && *buf != '-' && *buf!= '*' && *buf ++ != '/' )  ;
	op =*buf++;
	
	count1 =0; // (
	count2 =0; // )
	do {
		if (*buf=='(') count1++;
		if (*buf==')') count2++;
		*expr2++=*buf++;
	} while (count1!=count2);
	*expr2='\0';
	
	return op;
}
int eval(char *buf) {
	char op;
	char expr1[SIZE],expr2[SIZE];

	if(*buf!='(') 
		return atoi(buf);
	op=partition(buf,expr1,expr2);
	switch(op) 
	{
		case '+':
			return eval(expr1)+eval(expr2); 
		case '-':
			return eval(expr1)-eval(expr2);
		case '*':
			return eval(expr1)*eval(expr2); 
		case '/':
			return eval(expr1)/eval(expr2);
	}
}

int main(int argc, char* argv[]) {
	
	//char s [SIZE] ="(1+((3-1)*(2+1)))";
	
	char *buf;
	int i=0;
	if (argc<2) {
		puts ("Usage: eval.exe expression");
		return 1;
	}
		
	buf=argv[1];
	
	while (*buf!='\0') fputc (*buf++, stdout);
	fputc ('\n',stdout);
	printf ("Result is %d\n", eval(argv[1]));

	return 0;
}