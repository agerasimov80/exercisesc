#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>


long getSum1 (short * p, long  n) {
	// *p - pointer to start of the array
	//  n - number of elements
	if (n==1) 
		return *p;
	else 
		return *p + getSum1 (p+1,n-1);
}
long getSum2 (short * p, long  n) {
	// *p - pointer to start of the array
	//  n - number of elements
	long i=0;
	long sum =0;
	for (;i<n;i++) sum += *p++;
	return sum;
}


int main (int argc, char *argv []) {
	
	short M =0;	
	long i =0, array_size =0, n=0;	// e- number of elements (short int - 2 bytes), n - array SIZE bytes
	short * p ;					// pointer to array of shorts
	long sum=0;
	clock_t begin, end;

	srand (time(0));

	/*if (argc <2) {
		puts ("Usage: sumelements M");
		return 1;
	}
	*/
	M = 12; //atoi (argv[1]);
	array_size =(long)pow(2.0,M);		// 'pow' operates doubles...
	p = (short *) malloc (array_size * sizeof (short int));						

	for (i=0;i<array_size;i++) *p++ = rand()%11;
	p-=array_size;									// return pointer to the starting position

	printf ("Allocated %li bytes for %li elements(type short int)\n", array_size * sizeof (short int), array_size);

	begin=clock();
	sum = getSum1(p,array_size);
	end=clock();

	printf ("Sum : %ld\n", sum);
	printf ("Time start : %ld ms\n", begin);
	printf ("Time finish : %ld ms\n", end);
	printf ("Time total : %.3f s\n", (float) (end-begin)/(CLOCKS_PER_SEC));

	begin=clock();
	sum = getSum2(p,array_size);
	end=clock();
	
	printf ("Sum : %ld\n", sum);
	printf ("Time start : %ld\n", begin);
	printf ("Time finish : %ld\n", end);
	printf ("Time total : %ld ms\n", end-begin);
	
	free(p);

	return 0;
}