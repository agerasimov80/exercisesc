#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define N 10
#define R 4  // - 50  ..... 50 

void fillInArrayOfInt (int * a, int n) {

	int i=0;

	srand(time(0));
	for (i=0;i<n;i++){
		a[i]=rand()%(R*2+1)-R;
	}
}



int main () {

	int  a [N], sum=0;
	int begin,end,i;

	fillInArrayOfInt (a,N);

	for (i=0,begin=-1,end=-1 ; i<N && ( begin==-1 || end==-1 ) ; i++)
	{
		if (begin == -1) 
			if (a[i]<0) begin =i;
		if (end == -1)
			if (a[N-i]>0) end =N-i;
	}

	for (i=begin;i<end+1; i++)
		sum+=a[i];

	puts ("Printing array elements"  );
	for (i=0;i<N;i++)
		printf("%d ", a[i]);
	putchar ('\n');

	puts ("Printing selected elements"  );
	for  (i=begin;i<end+1; i++)
		printf("%d ", a[i]);
	putchar ('\n');

	printf ("Sum : %d", sum);

	return 0;
}