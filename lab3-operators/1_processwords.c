#include <stdio.h>
#include <string.h>


void chomp (char str[]){
	if (str[strlen(str)-1] =='\n')
		str[strlen(str)-1] ='\0';

}

void print(int countChars)
{
	printf ("  contains %02d character(s), \n",countChars);
}

int main () {

	
	char str [256] ;
	int countWords=0;
	int countChars=0;
	int inWord=0;
	char *c = str;

	puts ("Please enter a string ...");
	fgets (str,256,stdin);
	// "   sdcsddcsc    dssdcsdc   sdsd"
	chomp (str);


	while (*c)
	{
		
		if (*c==' ' && inWord==1) {
			inWord=0;					//end of word
			print(countChars);
			countChars=0;
		}

		else if(*c==' ' && inWord==0) 
					;			// idling
		else if(*c!=' ' && inWord==1) {
			printf ("%c",*c);
			countChars++;				//count characters
		}
		else if(*c!=' ' && inWord==0) {
			//countWords++;				//begining of the word!
			printf("%02d ",++countWords);
			printf ("%c",*c);
			countChars++;
			inWord=1;
		}

		c++;
		
	}
	if(inWord)
		print(countChars);

	printf ("Number of words : %d.\n",countWords);


		return 0;
}