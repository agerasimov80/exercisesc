#include <stdio.h>
#include <string.h>
#include <math.h>
#define length 9

void chomp (char * str) {
	if ( *(str+strlen(str)-1) == '\n') 
		 *(str+strlen(str)-1) = '\0';
}
int hasDigits(char * str ){
	char * temp = str;
	char * stringEnd = str + strlen(str)-1;
	while (*temp){
		if ( *temp >='0' &&  *temp <='9') {
			return 1;
		}
		temp++;
	}
	return 0;
}
int isDigit (char c) {
	if (c>='0' && c <='9') 
		return 1;
	else 
		return 0;
}

int sumIntElements (char  n [], int digits){
	int i=0;
	int sum=0;
	int t =0;
	for (i=0;i<digits; i++){
		t=pow(10.0,digits-i-1);
		sum+= (n[i] -'0') * t;
	}

	return sum;
}
void setToNullElements (char  n [], int len){
	int i;
	for(i=0;i<len;i++) n[i] ='0';
}

int main () {
	// sample string : " I am writing a 3-d programm for 30 minutes already...  "

	char buf [256];
	int isNumber =0;
	int sum=0;
	int numberOfDigits=0;
	int i=0;
	char number [length]={'0','0','0','0','0','0','0','0','0'};

	while (1) {
		fflush(stdin);
		puts ("Please enter a string containig at least 1 digit or a number ");
		fgets (buf, 256, stdin);
		chomp (buf);
		if (hasDigits(buf)) break;
	}
	puts ("Nice.It has digits.");
	puts ("Let's count the sum of numbers in the string."); 
	puts ("Each number couldn't be longer than 9 digits.");
	puts ("In case it is - the remaining part will be cut-off and counted separately.");

	while (buf [i]) {
		if (isDigit(buf[i]) && !isNumber) {			// starting the number 
			isNumber=1;
			numberOfDigits++;
			number [numberOfDigits-1] = buf[i];

		}
		else if (isDigit(buf[i]) && isNumber) {		//continue a number
			numberOfDigits++; 
			number [numberOfDigits-1] = buf[i];
		}
		else if (!isDigit(buf[i]) && isNumber) {	//end of the number 		
			sum+=sumIntElements (number, numberOfDigits);	// ������������ ��������� �������
			setToNullElements (number, length);		// ������� �������
			isNumber=0;
			numberOfDigits=0;
		}
		else if (!isDigit(buf[i]) && !isNumber){ 	//..
			; // do nothing
		}
		i++;
	}

	if (isNumber) {
		sum+=sumIntElements (number, numberOfDigits);
	}

	puts ("Done.");
	printf("Sum = %d\n", sum);

	return 0;
}