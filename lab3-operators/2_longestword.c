#include <stdio.h>
#include <string.h>


void chomp (char str[]){
	if (str[strlen(str)-1] =='\n')
		str[strlen(str)-1] ='\0';
}
void print(int countChars)
{
	printf (" - has %02d char(s), \n",countChars);
}
char * maxWordPointer(int countChars, char *c)
{
	static int maxCharacters=0;
	char * maxWord=NULL;
	if (countChars>maxCharacters){
			maxCharacters=countChars;
			maxWord=c;
	}
	return maxWord;
}

int main () {

	
	char str [256] ;
	int countWords=0;
	int countChars=0;
	int longestWordCharactersCount=0;
	int inWord=0;
	char *c = str;
	char *maxWordP =NULL ;
	char *temp;

	puts ("Please enter a string ...");
	fgets (str,256,stdin);
	// "   sdcsddcsc    dssdcsdc   sdsd"
	chomp (str);


	while (*c)
	{
		
		if (*c==' ' && inWord==1) {
			inWord=0;					//end of word
			print(countChars);	// print number of characters
			temp = maxWordPointer(countChars,temp); // send to maxWordPointer function the number of chars and pointer to the first char
			if (temp != NULL) maxWordP = temp;
			countChars=0;				// set countChars to null
		}

		else if(*c==' ' && inWord==0)	
					;					// idling, while between words
		else if(*c!=' ' && inWord==1) {
			printf ("%c",*c);			//print the current char
			countChars++;				//count characters
		}
		else if(*c!=' ' && inWord==0) {
			temp=c;
			printf("%02d ",++countWords);	//begining of the word!
			printf ("%c",*c);
			countChars++;
			inWord=1;
		}
		c++;
		
	}
	if(inWord)
	{
		print(countChars);
		temp = maxWordPointer(countChars,temp); // send to maxWordPointer function the number of chars and pointer to the first char
		if (temp != NULL) maxWordP = temp;
	}

	printf ("Number of words : %d. \n",countWords);
	printf ("Longest word : ");
	countChars=0;
	while (*maxWordP){
			printf ("%c",*maxWordP) ;
			if (*maxWordP==' ')
				break;
			maxWordP++;
			countChars++;
	}
	print(countChars);

		return 0;
}