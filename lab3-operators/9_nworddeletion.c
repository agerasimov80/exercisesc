#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 256


int setPointerToNWord (char *s  , char ** r, int n){
	int count =0;
	int inWord =0;

	while ( *s !=0 &&  ( n? (count < n):1) ){		// if n== 0 --> then 1
		if (*s != 32 && inWord ) {		// continue in a word

		}
		if (*s == 32 && inWord ) {		// faced end of the word
			inWord =0;
		}

		if (*s != 32 && !inWord ) {		// faced a word
			inWord=1;
			count++ ;
			*r = s;
		}
		if (*s == 32 && !inWord ) {		// spaces around

		}
		s++;
	}
	return count;  // return number of words
 }

void chomp (char * str)  {
	if (str[strlen(str)-1]=='\n')
		str[strlen(str)-1] =0;
}


int main () {

	char buf [N];
	int n=0;
	int res=0;
	int countW = 0;
	char * p ;
	char * p1;
	char * currentSymbol;

	// Input a String containing more than 1 word.
	do 	{
		puts ("Please enter a string:");
		fflush (stdin) ;
		fgets(buf, N, stdin);
		chomp (buf);
		fflush (stdin);
		countW=setPointerToNWord (buf, &p, 0); // count number of words & set pointer p to the last word
	} while (countW <1);

	do {
		printf ("Please enter a number 1..%d\n", countW );
		fflush (stdin);
		res = scanf ("%d", &n );
		fflush (stdin);
		if ( res ==1 && countW<n) printf ("Number should be less or equal %d!\n", countW);
	} while (res!=1 || n>countW );


	res = setPointerToNWord (buf , &p,  n);	// set pointer p to n-word

	currentSymbol = buf;
	if (n<countW) {
		do {
			if (currentSymbol == p) setPointerToNWord( buf, &currentSymbol, n+1);	// if faced n-word set pointer to n+1 word
			putchar (*currentSymbol);
			currentSymbol ++;
		} while (*currentSymbol !=0);   // while not ' '
	}
	else if (n==countW) {
		setPointerToNWord( buf, &p1, n-1);  // set pointer to n-1 word
		do {
			putchar (*currentSymbol);
			currentSymbol ++;
		} while (*currentSymbol !=' ' ||   currentSymbol - p1 <0);  // while not ' ' and not faced n-1 word
	}

	return 0;
}
