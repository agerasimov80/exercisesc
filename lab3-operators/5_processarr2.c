#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define N 10
#define R 4  // - 50  ..... 50 

void fillInArrayOfInt (int * a, int n) {

	int i=0;

	srand(time(0));
	for (i=0;i<n;i++){
		a[i]=rand()%(R*2+1)-R;
	}
}

int main () {

	int  a [N], sum=0;
	int begin,end,i;
	int min , max, minindex, maxindex;

	fillInArrayOfInt (a,N);

	for (i=0, minindex=-1,maxindex=-1, min=a[0], max=a[N-1] ; i<N ; i++)
	{
		if (a[i]<min) {
			minindex =i;
			min=a[i];
		}
		if (a[N-i]>max) {
			maxindex =N-i;
			max= a[N-i];
		}
	}

	begin = minindex<maxindex ? minindex:maxindex;
	end	  =	minindex<maxindex ? maxindex:minindex;
	
	for (i=begin;i<end+1; i++)
		sum+=a[i];

	puts ("Printing array elements"  );
	for (i=0;i<N;i++)
		printf("%d ", a[i]);
	putchar ('\n');

	puts ("Printing selected elements"  );
	for  (i=begin;i<end+1; i++)
		printf("%d ", a[i]);
	putchar ('\n');

	printf ("Sum : %d\n", sum);

	return 0;
}